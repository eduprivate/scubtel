#Solução:

Para a aplicação foi escolhido o springboot para desenvolver os serviços. Boa documentação, fácil utilização e fácil 
transporte (pacote jar, deploys mais ageis).

O desenvolvimento foi baseado em TDD, ou seja, primeiro os testes depois a implementação. 

Para o front-end escolhi o AngularJS, pois apresenta um ótimo modelo para construção das views consumindo serviços. 
O conteúdo do front-end esta junto (embedded) do projeto para facilitar a avaliação. Para projetos em produção essa parte 
poderia ser separada. 

De fato essa separação é facilita distribuir e escalar uma aplicação (Arquiteturas Reactive)

#Banco de dados
Na raiz do projeto esta disponível o script SQL para criação schema e popular as tabelas: sql/scubtel.sql.
Como alternativa a essa opção você, pode se ,preferir criar apenas o schema: mysql> create databases scubtel e 
acessar o entry-point (disponível  para ajudar a avaliação): http://localhost:8080/simulation/prepare

-Obs.: Lembre-se de alterar o senha do seu banco de dados local em application.properties

#Requisitos:
JRE - Java 7
Usada: Java(TM) SE Runtime Environment (build 1.7.0_79-b15)

#Build:
$ mvn clean package

#Execução
$ java -jar target/scubtel.jar 

#Página Inicial 
http://localhost:8080/

#Principais Entry-points
GET - http://localhost:8080/simulation

GET - http://localhost:8080/plans

GET - http://localhost:8080/prices

#Ou acesse pelo AWS
http://ec2-52-27-120-59.us-west-2.compute.amazonaws.com/
