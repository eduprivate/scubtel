var postApp = angular.module('scub', []);
// Controller function and passing $http service and $scope var.
postApp.controller('scubController', function($scope, $http) {
    $http({
      method  : 'GET',
      url     : 'http://localhost:8080/plans',
     })
      .success(function(data) {
    	  $scope.plans = data;
      });
    
    $http({
        method  : 'GET',
        url     : 'http://localhost:8080/prices',
       })
        .success(function(data) {
      	  $scope.prices = data;
        });
    
    $scope.submit = function() {
    	var planId = $scope.dataForm.planId;
    	var priceId = $scope.dataForm.priceId;
    	var callTime = $scope.dataForm.callTime;
        $http({
          method  : 'GET',
          url     : 'http://localhost:8080/simulation/'+planId+'/'+priceId+'/'+callTime,
         })
          .success(function(data) {
        	  $scope.simulation = data;
        	  console.log($scope.simulation)
          });
        };
});
