package br.com.scubtel.controller;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.scubtel.entity.Price;
import br.com.scubtel.service.PricesService;

@RestController
public class PricesController {
	
	private PricesService pricesService;
	private Logger logger = LogManager.getLogger(PricesController.class);
	
	@RequestMapping(value="/prices", method = RequestMethod.POST )
    public ResponseEntity<Price> createPrice(@RequestBody Price price) {
    	try {
    		pricesService.createPrice(price);
			return new ResponseEntity<Price>(price, HttpStatus.OK);
		} catch (NumberFormatException e) {
			logger.error("An error occurred while saving Price. ", e);
		} 
    	return new ResponseEntity<Price>( HttpStatus.PRECONDITION_FAILED); 
    }
	
	@RequestMapping(value="/prices", method = RequestMethod.GET )
    public @ResponseBody List<Price> getPrices() {
		return pricesService.listPrices();
    }
	
	@Autowired
	public void setPricesService(PricesService pricesService) {
		this.pricesService = pricesService;
	}
}
