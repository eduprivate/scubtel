package br.com.scubtel.controller;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.scubtel.entity.Plan;
import br.com.scubtel.service.PlansService;

@RestController
public class PlansController {
	
	private PlansService plansService;
	private Logger logger = LogManager.getLogger(PlansController.class);
	
	@RequestMapping(value="/plans", method = RequestMethod.POST )
    public ResponseEntity<Plan> createPlan(@RequestBody Plan plan) {
    	try {
    		plansService.createPlan(plan);
			return new ResponseEntity<Plan>(plan, HttpStatus.OK);
		} catch (NumberFormatException e) {
			logger.error("An error occurred while saving plan. ", e);
		} 
    	return new ResponseEntity<Plan>( HttpStatus.PRECONDITION_FAILED); 
    }
	
	@RequestMapping(value="/plans", method = RequestMethod.GET )
    public @ResponseBody List<Plan> getPlans() {
		return plansService.listPlans();
    }
	
	@Autowired
	public void setPlansService(PlansService plansService) {
		this.plansService = plansService;
	}
}
