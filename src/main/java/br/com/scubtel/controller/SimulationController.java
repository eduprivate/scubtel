package br.com.scubtel.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.scubtel.entity.Plan;
import br.com.scubtel.entity.Price;
import br.com.scubtel.entity.Simulation;
import br.com.scubtel.service.PlansService;
import br.com.scubtel.service.PricesService;

@RestController
public class SimulationController {
	
	private PlansService plansService;
	private PricesService pricesService;
	private Logger logger = LogManager.getLogger(SimulationController.class);
	
	@RequestMapping(value="/simulation", method = RequestMethod.GET, consumes={MediaType.APPLICATION_JSON_VALUE} )
    public ResponseEntity<Simulation> simulatePost(@RequestBody Long planId, 
    		@RequestBody Long priceId, 
    		@RequestBody Double callTime) {
    	
		try {
    		Plan plan = plansService.findById(planId);
    		Price price = pricesService.findById(priceId);
    		Simulation simulation = new Simulation();
    		
    		simulation.simulate(plan, price, callTime);
    		
    		return new ResponseEntity<Simulation>(simulation, HttpStatus.ACCEPTED);
    		
		} catch (NumberFormatException e) {
			logger.error("An error occurred when updating position. ", e);
		}
    	return new ResponseEntity<Simulation>(HttpStatus.PRECONDITION_FAILED); 
    }
	
	@RequestMapping(value="/simulation/{planId}/{priceId}/{callTime}", method = RequestMethod.GET )
    public ResponseEntity<Simulation> simulate(@PathVariable("planId") Long planId, 
    		@PathVariable("priceId") Long priceId, 
    		@PathVariable("callTime") Double callTime) {
    	
		try {
    		Plan plan = plansService.findById(planId);
    		Price price = pricesService.findById(priceId);
    		Simulation simulation = new Simulation();
    		
    		simulation.simulate(plan, price, callTime);
    		
    		return new ResponseEntity<Simulation>(simulation, HttpStatus.ACCEPTED);
    		
		} catch (NumberFormatException e) {
			logger.error("An error occurred when updating position. ", e);
		}
    	return new ResponseEntity<Simulation>(HttpStatus.PRECONDITION_FAILED); 
    }
	
	@RequestMapping(value="/simulation/prepare", method = RequestMethod.GET )
    public ResponseEntity<String> prepareData() {
    	
		try {
			
			Plan plan1 = new Plan("Plano 30", 30d);
			Plan plan2 = new Plan("Plano 60", 60d);
			Plan plan3 = new Plan("Plano 120", 120d);
			
			Price price1 = new Price("011", "016", 1.9d);
			Price price2 = new Price("016", "011", 2.9d);
			Price price3 = new Price("011", "017", 1.7d);
			Price price4 = new Price("017", "011", 2.7d);
			Price price5 = new Price("011", "018", 0.9d);
			Price price6 = new Price("018", "011", 1.9d);
			
			plansService.createPlan(plan1);
			plansService.createPlan(plan2);
			plansService.createPlan(plan3);
			
			pricesService.createPrice(price1);
			pricesService.createPrice(price2);
			pricesService.createPrice(price3);
			pricesService.createPrice(price4);
			pricesService.createPrice(price5);
			pricesService.createPrice(price6);
    		
    		return new ResponseEntity<String>(HttpStatus.ACCEPTED);
    		
		} catch (NumberFormatException e) {
			logger.error("An error occurred when updating position. ", e);
		}
    	return new ResponseEntity<String>(HttpStatus.PRECONDITION_FAILED); 
    }

	
	@Autowired
	public void setPlansService(PlansService plansService) {
		this.plansService = plansService;
	}
	
	@Autowired
	public void setPricesService(PricesService pricesService) {
		this.pricesService = pricesService;
	}
}
