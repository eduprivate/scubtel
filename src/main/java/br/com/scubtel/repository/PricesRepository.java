package br.com.scubtel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scubtel.entity.Price;

public interface PricesRepository extends JpaRepository<Price, Long> {

	 Price findById(Long id);
	/* 
	 @Query(value="select p.origem from prices p",nativeQuery = true)
	 List<String> listOrigins();
	 
	 @Query(value="select p.destiny from prices p where p.origin = :origin ",nativeQuery = true)
	 List<String> listDestinys(@Param("origin") String origin);
	 
	 @Query(value="select * from prices p where p.origin = :origin ",nativeQuery = true)
	 List<String> listDestinys(@Param("origin") String origin, @Param("destiny") String destiny);
	*/
}
