package br.com.scubtel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scubtel.entity.Plan;

public interface PlansRepository extends JpaRepository<Plan, Long> {

	 Plan findById(Long id);
	
}
