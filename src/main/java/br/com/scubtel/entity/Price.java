package br.com.scubtel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "Prices", uniqueConstraints=
@UniqueConstraint(columnNames = {"origin", "destiny"}))
public class Price implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name="origin")
	private String origin;
	
	@NotNull
	@Column(name="destiny")
	private String destiny;
	
	@NotNull
	@Column(name="price")
	private Double price;
	
	public Price() {
	}
	
	public Price(String origin, String destiny, Double price) {
		super();
		this.origin = origin;
		this.destiny = destiny;
		this.price = price;
	}

	public Price(Long id, String origin, String destiny, Double price) {
		super();
		this.id = id;
		this.origin = origin;
		this.destiny = destiny;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestiny() {
		return destiny;
	}

	public void setDestiny(String destiny) {
		this.destiny = destiny;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}