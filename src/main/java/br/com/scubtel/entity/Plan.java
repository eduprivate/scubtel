package br.com.scubtel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "Plans", uniqueConstraints=
@UniqueConstraint(columnNames = {"plan_name", "plan_minutes"}))
public class Plan implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Column(name="plan_name")
	private String name;
	
	@NotNull
	@Column(name="plan_minutes")
	private Double minutes;
	
	public Plan() {
	}
	
	public Plan(String name, Double minutes) {
		super();
		this.name = name;
		this.minutes = minutes;
	}

	public Plan(Long id, String name, Double minutes) {
		super();
		this.id = id;
		this.name = name;
		this.minutes = minutes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMinutes() {
		return minutes;
	}

	public void setMinutes(Double minutes) {
		this.minutes = minutes;
	}

}