package br.com.scubtel.entity;

public class Simulation {
	private Double valueWithPlan;
	private Double valueWithoutPlan;
	
	public Simulation() {
	}

	public Simulation(Double valueWithPlan, Double valueWithoutPlan) {
		super();
		this.valueWithPlan = valueWithPlan;
		this.valueWithoutPlan = valueWithoutPlan;
	}

	public Double getValueWithPlan() {
		return valueWithPlan;
	}

	public void setValueWithPlan(Double valueWithPlan) {
		this.valueWithPlan = valueWithPlan;
	}

	public Double getValueWithoutPlan() {
		return valueWithoutPlan;
	}

	public void setValueWithoutPlan(Double valueWithoutPlan) {
		this.valueWithoutPlan = valueWithoutPlan;
	}
	
	public void simulate(Plan plan, Price price, Double callTime) {
		valueWithoutPlan = callTime * price.getPrice();
		
		if (callTime <= plan.getMinutes()){
			valueWithPlan = 0.0d;
		} else {
			Double usedPricePlan = (callTime - plan.getMinutes()) * price.getPrice();
			valueWithPlan = usedPricePlan + (0.1 * usedPricePlan);
		}
	}
	
}
