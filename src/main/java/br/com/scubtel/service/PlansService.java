package br.com.scubtel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.scubtel.entity.Plan;
import br.com.scubtel.repository.PlansRepository;

@Service
public class PlansService {
	
	private PlansRepository plansRepository;
	
	public List<Plan> listPlans(){
		return plansRepository.findAll();
	}
	
	public Plan findById(Long id){
		return plansRepository.findById(id);
	}
	
	public void createPlan(Plan plan) {
		plansRepository.save(plan);
	}
	
	public void deletePlan(Plan plan) {
		plansRepository.delete(plan);
	}
	
	@Autowired
	public void setPlansRepository(PlansRepository plansRepository) {
		this.plansRepository = plansRepository;
	}
}
