package br.com.scubtel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.scubtel.entity.Price;
import br.com.scubtel.repository.PricesRepository;

@Service
public class PricesService {
	
	private PricesRepository pricesRepository;
	
	public List<Price> listPrices(){
		return pricesRepository.findAll();
	}
	
/*	public List<String> listOrigins(){
		return pricesRepository.listOrigins();
	}*/
	
	public void createPrice(Price price) {
		pricesRepository.save(price);
	}
	
	public void deletePrice(Price price) {
		pricesRepository.delete(price);
	}
	
	public Price findById(Long priceId) {
		return this.pricesRepository.findById(priceId);
	}
	
	@Autowired
	public void setPricesRepository(PricesRepository pricesRepository) {
		this.pricesRepository = pricesRepository;
	}

	
}
