package br.com.scubtel.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import br.com.scubtel.Application;
import br.com.scubtel.entity.Plan;
import br.com.scubtel.repository.PlansRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
@IntegrationTest
public class PlansControllerTest {

	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private RestTemplate restTemplate = new TestRestTemplate();
	   
	PlansRepository repository; 

	@Test
	public void savePlanTest() throws JsonProcessingException {
		// Give
		Map<String, Object> requestCreatePlan = new HashMap<String, Object>();
		requestCreatePlan.put("name", "FaleMais 220");
		requestCreatePlan.put("minutes", "120");

		// When
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Plan> responseCreate = restTemplate.postForEntity(
				"http://localhost:8080/plans/", requestCreatePlan, Plan.class,
				Collections.EMPTY_MAP);
		
		Plan plan = responseCreate.getBody();
		repository.delete(plan);

		// Then
		assertNotNull(responseCreate);
		assertEquals(HttpStatus.OK, responseCreate.getStatusCode());

	}

	@Test
	public void getPlansListTest() throws JsonProcessingException {

		// Give
		Map<String, Object> requestCreatePlan1 = new HashMap<String, Object>();
		requestCreatePlan1.put("name", "FaleMais 330");
		requestCreatePlan1.put("minutes", "30");
		
		Map<String, Object> requestCreatePlan2 = new HashMap<String, Object>();
		requestCreatePlan2.put("name", "FaleMais 660");
		requestCreatePlan2.put("minutes", "60");

		// When
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Plan> responseCreate1 = restTemplate.postForEntity(
				"http://localhost:8080/plans/", requestCreatePlan1, Plan.class,
				Collections.EMPTY_MAP);
		
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Plan> responseCreate2 = restTemplate.postForEntity(
				"http://localhost:8080/plans/", requestCreatePlan2, Plan.class,
				Collections.EMPTY_MAP);
		
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<List> responseStatus = restTemplate.getForEntity(
				"http://localhost:8080/plans/", List.class,
				Collections.EMPTY_MAP);
		
		List<Plan> plans = responseStatus.getBody();

		// Then
		assertNotNull(responseStatus);
		assertEquals(HttpStatus.OK, responseStatus.getStatusCode());
		assertTrue(plans.size() > 0);
		
		plans = repository.findAll();
		for (Plan plan : plans) {
			repository.delete(plan);
		}		
	}
	
	@Autowired
	public void setRepository(PlansRepository repository) {
		this.repository = repository;
	}

}
