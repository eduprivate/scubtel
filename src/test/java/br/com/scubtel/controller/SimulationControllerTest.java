package br.com.scubtel.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import br.com.scubtel.Application;
import br.com.scubtel.entity.Plan;
import br.com.scubtel.entity.Price;
import br.com.scubtel.entity.Simulation;
import br.com.scubtel.service.PlansService;
import br.com.scubtel.service.PricesService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
@IntegrationTest
public class SimulationControllerTest {

	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private RestTemplate restTemplate = new TestRestTemplate();
	private PlansService plansService;
	private PricesService pricesService;
	
	@Test
	public void simulatePlan30Test() throws JsonProcessingException {
		// Given
		Plan plan = new Plan("Plano 330", 30d);
		Price price = new Price("021", "026", 1.9d);
		Double expectedValueWithoutPlan = new Double(38d);
		Double expectedValueWithPlan = new Double(0d);
		Double callTime = new Double(20d);

		// When
		plansService.createPlan(plan);
		pricesService.createPrice(price);

		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Simulation> response = restTemplate.getForEntity(
				"http://localhost:8080/simulation/" + plan.getId() +"/"+price.getId()+"/"+callTime, Simulation.class, Collections.EMPTY_MAP);
		Simulation simulation = response.getBody();
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		// Then
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());

	}

	@Test
	public void simulatePlan60Test() throws JsonProcessingException {
		// Given
		Plan plan = new Plan("Plano 660", 60d);
		Price price = new Price("021", "027", 1.7d);
		Double expectedValueWithoutPlan = new Double(136.00d);
		Double expectedValueWithPlan = new Double(37.40d);
		Double callTime = new Double(80d);

		// When
		plansService.createPlan(plan);
		pricesService.createPrice(price);

		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Simulation> response = restTemplate.getForEntity(
				"http://localhost:8080/simulation/" + plan.getId() +"/"+price.getId()+"/"+callTime, Simulation.class, Collections.EMPTY_MAP);
		Simulation simulation = response.getBody();
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		// Then
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());

	}
	
	@Test
	public void simulatePlan120Test() throws JsonProcessingException {
		// Given
		Plan plan = new Plan("Plano 220", 120d);
		Price price = new Price("028", "021", 1.9d);
		Double expectedValueWithoutPlan = new Double(380.00d);
		Double expectedValueWithPlan = new Double(167.20d);
		Double callTime = new Double(200d);

		// When
		plansService.createPlan(plan);
		pricesService.createPrice(price);

		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Simulation> response = restTemplate.getForEntity(
				"http://localhost:8080/simulation/" + plan.getId() +"/"+price.getId()+"/"+callTime, Simulation.class, Collections.EMPTY_MAP);
		Simulation simulation = response.getBody();
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		// Then
		assertNotNull(response);
		assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());

	}

	@Autowired
	public void setPlansService(PlansService plansService) {
		this.plansService = plansService;
	}

	@Autowired	
	public void setPricesService(PricesService pricesService) {
		this.pricesService = pricesService;
	}
}
