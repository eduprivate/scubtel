package br.com.scubtel.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import br.com.scubtel.Application;
import br.com.scubtel.entity.Price;
import br.com.scubtel.repository.PricesRepository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
@IntegrationTest
public class PricesControllerTest {

	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private RestTemplate restTemplate = new TestRestTemplate();
	   
	PricesRepository repository; 

	@Test
	public void savePriceTest() throws JsonProcessingException {
		// Give
		Map<String, Object> requestCreatePrice = new HashMap<String, Object>();
		requestCreatePrice.put("origin", "021");
		requestCreatePrice.put("destiny", "028");
		requestCreatePrice.put("price", "1.9");

		// When
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Price> responseCreate = restTemplate.postForEntity(
				"http://localhost:8080/prices/", requestCreatePrice, Price.class,
				Collections.EMPTY_MAP);
		
		Price price = responseCreate.getBody();
		repository.delete(price);

		// Then
		assertNotNull(responseCreate);
		assertEquals(HttpStatus.OK, responseCreate.getStatusCode());

	}

	@Test
	public void getPriceListTest() throws JsonProcessingException {

		// Give
		Map<String, Object> requestCreatePrice1 = new HashMap<String, Object>();
		requestCreatePrice1.put("origin", "021");
		requestCreatePrice1.put("destiny", "028");
		requestCreatePrice1.put("price", "1.9");
		
		Map<String, Object> requestCreatePrice2 = new HashMap<String, Object>();
		requestCreatePrice2.put("origin", "026");
		requestCreatePrice2.put("destiny", "021");
		requestCreatePrice2.put("price", "2.9");
		// When
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Price> responseCreate1 = restTemplate.postForEntity(
				"http://localhost:8080/prices/", requestCreatePrice1, Price.class,
				Collections.EMPTY_MAP);
		
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<Price> responseCreate2 = restTemplate.postForEntity(
				"http://localhost:8080/prices/", requestCreatePrice2, Price.class,
				Collections.EMPTY_MAP);
		
		@SuppressWarnings({ "unchecked" })
		ResponseEntity<List> responseStatus = restTemplate.getForEntity(
				"http://localhost:8080/prices/", List.class,
				Collections.EMPTY_MAP);
		
		List<Price> prices = responseStatus.getBody();

		// Then
		assertNotNull(responseStatus);
		assertEquals(HttpStatus.OK, responseStatus.getStatusCode());
		assertTrue(prices.size() > 0);
		
		prices = repository.findAll();
		for (Price price : prices) {
			repository.delete(price);
		}		
	}
	
	@Autowired
	public void setRepository(PricesRepository repository) {
		this.repository = repository;
	}

}