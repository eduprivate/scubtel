package br.com.scubtel.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.scubtel.Application;
import br.com.scubtel.entity.Plan;
import br.com.scubtel.entity.Price;
import br.com.scubtel.entity.Simulation;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
@IntegrationTest
public class SimulatingTest {
	
	private PlansService plansService;
	private PricesService pricesService;

	@Test
	public void simulatingPlan30Test() {
		// Given
		Plan plan = new Plan("Plano 330", 30d);
		Price price = new Price("021", "026", 1.9d);
		Double expectedValueWithoutPlan = new Double(38d);
		Double expectedValueWithPlan = new Double(0d);
		Double callTime = new Double(20d);
		
		// When 
		plansService.createPlan(plan);
		pricesService.createPrice(price);
		
		Simulation simulation = new Simulation();
		simulation.simulate(plan, price, callTime);
		
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());

	}
	
	@Test
	public void simulatingPlan60Test() {
		// Given
		Plan plan = new Plan("Plano 660", 60d);
		Price price = new Price("021", "027", 1.7d);
		Double expectedValueWithoutPlan = new Double(136.00d);
		Double expectedValueWithPlan = new Double(37.40d);
		Double callTime = new Double(80d);
		
		// When 
		plansService.createPlan(plan);
		pricesService.createPrice(price);
		
		Simulation simulation = new Simulation();
		simulation.simulate(plan, price, callTime);
		
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());

	}
	
	@Test
	public void simulatingPlan120Test() {
		// Given
		Plan plan = new Plan("Plano 220", 120d);
		Price price = new Price("028", "021", 1.9d);
		Double expectedValueWithoutPlan = new Double(380.00d);
		Double expectedValueWithPlan = new Double(167.20d);
		Double callTime = new Double(200d);
		
		// When 
		plansService.createPlan(plan);
		pricesService.createPrice(price);
		
		Simulation simulation = new Simulation();
		simulation.simulate(plan, price, callTime);
		
		plansService.deletePlan(plan);
		pricesService.deletePrice(price);
		
		assertTrue(simulation != null);
		assertEquals(expectedValueWithoutPlan, simulation.getValueWithoutPlan());
		assertEquals(expectedValueWithPlan, simulation.getValueWithPlan());
	
	}
	
	@Autowired
	public void setPlansService(PlansService plansService) {
		this.plansService = plansService;
	}
	
	@Autowired
	public void setPricesService(PricesService pricesService) {
		this.pricesService = pricesService;
	}

}
